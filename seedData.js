const path = require('path');

const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'aman_vj0896',
  port: 5432
});

function dropTable() {
  return pool.query('DROP TABLE IF EXISTS customers, orders');
}
function createTableCustomers() {
  return pool.query(
    'CREATE TABLE customers(customer_id INT PRIMARY KEY, customer_name VARCHAR(40), address VARCHAR(100), phone_no VARCHAR(20))'
  );
}
function createTableOrders() {
  return pool.query(
    'CREATE TABLE orders(order_id INT, customer_id INT, item_name VARCHAR(50), item_amount INT)'
  );
}
function insertCsvFileToCustomerTable() {
  const customerPath = getPathForCustomersCsv();
  return pool.query(
    `COPY customers FROM '${customerPath}' with delimiter ',' csv header`
  );
}
function getPathForCustomersCsv() {
  const customerpath = path.resolve('./', 'sampleData/customers.csv');
  return customerpath;
}
function insertCsvFileToOrdersTable() {
  const ordersPath = getPathForOrdersCsv();
  return pool.query(
    `COPY orders FROM '${ordersPath}' with delimiter ',' csv header`
  );
}
function getPathForOrdersCsv() {
  const orderspath = path.resolve('./', 'sampleData/orders.csv');
  return orderspath;
}
async function createTableAndInsertIntoTables() {
  await createTableCustomers();
  await insertCsvFileToCustomerTable();
  console.log('Successsfully created table customers');
  await createTableOrders();
  await insertCsvFileToOrdersTable();
  console.log('Successsfully created table orders');
}
dropTable().then(() => createTableAndInsertIntoTables());
