const orders = require('../models/orders');
const validation = require('../utils/validation.js');

const readOrderData = function(req, res, next) {
  orders
    .readOrdersData()
    .then(allValues => {
      if (allValues.length !== 0) {
        res.status(200).send(allValues);
      } else {
        res.status(404);
        next({
          message: 'No orders available',
          statusCode: 404
        });
      }
    })
    .catch(err => {
      res.status(400).send(err);
      next({ message: 'Bad Request', statusCode: 400 });
    });
};
const readOrderDataWithId = function(req, res, next) {
  const request = validation.schema1.validate(req.params);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    orders
      .readOrderDataWithId(req.params.id)
      .then(value => {
        if (value.length !== 0) {
          res.status(200).send(value);
        } else {
          res.status(404);
          next({
            message: `Order with id ${req.params.id} not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const readOrderDataWithCustomerId = function(req, res, next) {
  const request = validation.schema1.validate(req.params);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    orders
      .readOrderDataWithCustomerId(req.params.id)
      .then(value => {
        if (value.length !== 0) {
          res.send(value);
        } else {
          res.status(404);
          next({
            message: `Customer with id ${req.params.id} not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const insertOrderInOrdersTable = function(req, res, next) {
  const request = validation.schema3.validate(req.body);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    const insertNewOrder = Object.values(req.body);
    orders
      .insertOrderInOrdersTable(insertNewOrder)
      .then(() => res.status(200).send('New order inserted successfully'))
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const updateOrderInOrdersTable = function(req, res, next) {
  const requestId = validation.schema1.validate(req.params);
  const requestBody = validation.schema3.validate(req.body);
  if (requestId.error) {
    res.status(400).send(requestId.error.details[0].message);
    next(requestId.error.details[0].message);
  } else if (requestBody.error) {
    res.status(400).send(requestBody.error.details[0].message);
    next(requestBody.error.details[0].message);
  } else {
    const updateOrder = Object.values(req.body);
    updateOrder.push(parseInt(req.params.id));
    orders
      .updateOrderInOrdersTable(updateOrder)
      .then(() =>
        res
          .status(200)
          .send(`Order with id ${req.params.id} updated successfully`)
      )
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
const deleteOrderWithId = function(req, res, next) {
  const request = validation.schema1.validate(req.params);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else {
    orders
      .deleteOrderWithId(req.params.id)
      .then(data => {
        if (data.rowCount === 0) {
          res.status(404).send(`Order with id ${req.params.id} not available`);
        } else {
          res.status(200).send(`Deleted order with orderId ${req.params.id}`);
        }
      })
      .catch(err => {
        res.status(400).send(err);
        next({ message: 'Bad request', statusCode: 400 });
      });
  }
};
module.exports = {
  readOrderData,
  readOrderDataWithId,
  readOrderDataWithCustomerId,
  insertOrderInOrdersTable,
  updateOrderInOrdersTable,
  deleteOrderWithId
};
