const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'aman_vj0896',
  port: 5432
});
async function readCustomerData() {
  try {
    const customersData = await pool.query('SELECT * FROM customers');
    return customersData.rows;
  } catch (err) {
    return err;
  }
}
async function readCustomerDataWithId(customerId) {
  try {
    const customerWithId = await pool.query(
      'SELECT * FROM customers WHERE customer_id = $1',
      [customerId]
    );
    return customerWithId.rows;
  } catch (err) {
    return err;
  }
}
async function insertCustomerInCustomersTable(array) {
  try {
    return pool.query(
      `INSERT INTO customers(customer_id, customer_name, address, phone_no) 
    VALUES((SELECT MAX(customer_id) FROM orders)+1, $1, $2, $3)`,
      array
    );
  } catch (err) {
    return err;
  }
}
async function updateCustomerInCustomersTable(array) {
  try {
    return await pool.query(
      `UPDATE customers SET customer_name = $1, address = $2, phone_No = $3 WHERE customer_id = $4`,
      array
    );
  } catch (err) {
    return err;
  }
}
async function deleteCustomerWithId(customerId) {
  try {
    return await pool.query(`DELETE FROM customers WHERE customer_id = $1`, [
      customerId
    ]);
  } catch (err) {
    return err;
  }
}
module.exports = {
  readCustomerData,
  readCustomerDataWithId,
  insertCustomerInCustomersTable,
  updateCustomerInCustomersTable,
  deleteCustomerWithId
};