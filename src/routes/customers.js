const express = require('express');
const router = express.Router();
const db = require('../middleware/customers.js');

router.get('/', db.readCustomerData);
router.get('/:id', db.readCustomerWithId);
router.post('/', db.insertNewCustomer);
router.put('/:id', db.updateCustomerWithId);
router.delete('/:id', db.deleteCustomerWithId);
router.get('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
}) 
router.post('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
router.put('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
router.delete('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
module.exports = router;