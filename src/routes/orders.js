const express = require('express');
const router = express.Router();
const db = require('../middleware/orders.js');

router.get('/', db.readOrderData);
router.get('/:id', db.readOrderDataWithId);
router.get('/cId/:id', db.readOrderDataWithCustomerId);
router.post('/', db.insertOrderInOrdersTable);
router.put('/:id', db.updateOrderInOrdersTable);
router.delete('/:id',  db.deleteOrderWithId);
router.get('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
router.post('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
router.put('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
router.delete('*', function (req, res) { 
  res.status(404).send(`Invalid Url - 404 Page`); 
});
module.exports = router;