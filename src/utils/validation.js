const Joi = require('@hapi/joi');

const schema1 = Joi.object({
  id: Joi.number()
    .integer()
    .positive()
});

const schema2 = Joi.object({
  name: Joi.string()
    .min(3)
    .required(),
  address: Joi.string()
    .min(3)
    .required(),
  phoneNo: Joi.string()
    .regex(/^\d{10}/)
    .required()
});

const schema3 = Joi.object({
  customerId: Joi.number()
    .integer()
    .positive()
    .required(),
  itemName: Joi.string().required(),
  itemAmount: Joi.number()
    .positive()
    .greater(1)
    .precision(2)
    .required()
});

module.exports = { schema1, schema2, schema3 };
